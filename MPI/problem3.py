from mpi4py import MPI
import numpy as np

# Problem 3
"""In each process, generate a random number, then send that number to the
process with the next highest rank (the last process should send to the root).
Print what each process starts with and what each process receives.

Usage:
    $ mpiexec -n 2 python problem3.py
    Process 1 started with [ 0.79711384]        # Values and order will vary.
    Process 1 received [ 0.54029085]
    Process 0 started with [ 0.54029085]
    Process 0 received [ 0.79711384]

    $ mpiexec -n 3 python problem3.py
    Process 2 started with [ 0.99893055]
    Process 0 started with [ 0.6304739]
    Process 1 started with [ 0.28834079]
    Process 1 received [ 0.6304739]
    Process 2 received [ 0.28834079]
    Process 0 received [ 0.99893055]
"""
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()
num = np.random.rand(1)
print("Process {} ".format(RANK) + "started with {}".format(num))
#send and receive in a loop, with special cases at the last and first processor
if RANK == 0:
    #recieve from the last
    COMM.Send(num,dest=RANK+1)
    COMM.Recv(num,source = COMM.Get_size()-1)
    print("Process {} ".format(RANK) + "received {}".format(num))
elif RANK == COMM.Get_size()-1:
    #send to the first
    COMM.Send(num,dest=0)
    COMM.Recv(num,source = RANK-1)
    print("Process {} ".format(RANK) + "received {}".format(num))
else:
    #recieve from before, send to after
    COMM.Send(num,dest=RANK+1)
    COMM.Recv(num,source = RANK-1)
    print("Process {} ".format(RANK) + "received {}".format(num))
