from mpi4py import MPI
from sys import argv
import numpy as np

# Problem 2
"""Pass a random NumPy array of shape (n,) from the root process to process 1,
where n is a command-line argument. Print the array and process number from
each process.

Usage:
    # This script must be run with 2 processes.
    $ mpiexec -n 2 python problem2.py 4
    Process 1: Before checking mailbox: vec=[ 0.  0.  0.  0.]
    Process 0: Sent: vec=[ 0.03162613  0.38340242  0.27480538  0.56390755]
    Process 1: Recieved: vec=[ 0.03162613  0.38340242  0.27480538  0.56390755]
"""
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()
#assign the initial value given
n = int(argv[1])
a = np.zeros(n,dtype=float)
#print the vector of zeros first, then recieve something from the other process
if RANK == 1:
    print("Process {}:".format(RANK) + " Before checking mailbox: vec={}".format(a))
if RANK == 0:
    a = np.random.rand(n)
    #send the values that you drew
    print("Process {}:".format(RANK) + " Sent: vec={}".format(a))
    COMM.Send(a,dest=1)
if RANK == 1:
    #print the newly recieved things
    COMM.Recv(a,source=0)
    print("Process {}:".format(RANK) + " Recieved: vec={}".format(a))
