from mpi4py import MPI
from sys import argv
from scipy import linalg as la
import numpy as np

# Problem 4
"""The n-dimensional open unit ball is the set U_n = {x in R^n : ||x|| < 1}.
Estimate the volume of U_n by making N draws on each available process except
for the root process. Have the root process print the volume estimate.

Command line arguments:
    n (int): the dimension of the unit ball.
    N (int): the number of random draws to make on each process but the root.

Usage:
    # Estimate the volume of U_2 (the unit circle) with 2000 draws per process.
    $ mpiexec -n 4 python problem4.py 2 2000
    Volume of 2-D unit ball: 3.13266666667      # Results will vary slightly.
"""
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()
#assign the original values
dim = int(argv[1])
draws = int(argv[2])
if RANK == 0:
    #make a list that will append all the values given to
    x = []
    within = np.array([0])
    #receive for every processor that calculated something
    for k in range(COMM.Get_size()-2):

        COMM.Recv(within,source=MPI.ANY_SOURCE)
        x.append(within[0])
    #do the calculation for the area of the nball
    est = 2**dim * (np.sum(x)/(draws*(COMM.Get_size()-2)))
    print("Volume of a {}-D ".format(dim) + "unit ball: {}".format(est))
else:
    points = np.random.uniform(-1,1,(dim,draws))
    lengths = la.norm(points,axis=0)
    within = np.count_nonzero(lengths<1)
    COMM.Send(np.array([within]),dest=0)
