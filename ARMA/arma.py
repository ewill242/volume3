# Name
# Date
# Class

from scipy.stats import norm
from scipy.optimize import fmin
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.tsa.arima_model import ARMA
from pydataset import data as pydata
from statsmodels.tsa.stattools import arma_order_select_ic as order_select
import pandas as pd



def arma_forecast_naive(file='weather.npy',p=2,q=1,n=20):
    """
    Perform ARMA(1,1) on data. Let error terms be drawn from
    a standard normal and let all constants be 1.
    Predict n values and plot original data with predictions.

    Parameters:
        file (str): data file
        p (int): order of autoregressive model
        q (int): order of moving average model
        n (int): number of future predictions
    """
    weatherPoints = np.load(file)
    #set theta and phi for all info
    phi = .5
    theta = .1
    zt = weatherPoints[1:] - weatherPoints[:-1]
    N = len(zt)
    newPoints = np.zeros(n)
    #create the epsillons to use for each step in the prediction
    noise = np.random.normal(loc = 0,scale=1.0,size=n+q)
    #calculate the domain to use for weather
    start = 13.79166
    finish = 13.79166 + N*(1./24.)
    finishPred = finish + n*(1./24.)
    #combine the predictions and original data to make the next part easier
    complete = np.hstack((zt,newPoints))
    #use an arma(p,q) method to get the next steps
    for a in range(n):
        complete[N+a] = sum(phi*complete[N-p:N]) + noise[a+q] + sum(theta*(noise[q-(a+q):(a+q-1)]))
    plt.figure(figsize=(10,4))
    #graph the results
    plt.plot(np.linspace(start,finish,N,endpoint=True),zt[:N],label="Old Data",color='b')
    plt.plot(np.linspace(finish,finishPred,n,endpoint=True)+(1./24.),complete[N:],label="New Data",color='orange')
    plt.ylabel("Change in Tempurature")
    plt.xlabel("Day of the Month")
    plt.title("ARMA({},{}) Model on change in Temperature with {} predicted changes".format(p,q,n))
    plt.gca().legend()
    plt.show()

    return


def arma_likelihood(file='weather.npy', phis=np.array([0]), thetas=np.array([0]), mu=0., std=1.):
    """
    Transfer the ARMA model into state space. 
    Return the log-likelihood of the ARMA model.

    Parameters:
        file (str): data file
        phis (ndarray): coefficients of autoregressive model
        thetas (ndarray): coefficients of moving average model
        mu (float): mean of errorm
        std (float): standard deviation of error

    Return:
        log_likelihood (float)
    """
    weatherPoints = np.load(file)
    zt = weatherPoints[1:] - weatherPoints[:-1]
    liklihood = []
    F,Q,H,dim_states,dim_time_series = state_space_rep(phis=phis, thetas=thetas, mu=mu, sigma=std)
    #calculate the mus and covariances to used to calculate the liklihoods
    mus,covs = kalman(F,Q,H,zt-mu)

    for i in range(len(zt)):
        men = H@mus[i]+mu
        var = H@covs[i]@H.T
        liklihood.append(norm.pdf(zt[i],loc=men,scale=np.sqrt(var)))
    #take the product of the logs of the liklihoods by summing
    return sum(np.log(liklihood))

def model_identification(file='weather.npy',i=4,j=4):
    """
    Identify parameters to minimize AIC of ARMA(p,q) model

    Parameters:
        file (str): data file
        p (int): maximum order of autoregressive model
        q (int): maximum order of moving average model

    Returns:
        phis (ndarray (p,)): coefficients for AR(p)
        thetas (ndarray (q,)): coefficients for MA(q)
        mu (float): mean of error
        std (float): std of error
    """
    weatherPoints = np.load(file)
    zt = weatherPoints[1:] - weatherPoints[:-1]
    sol = []
    n=len(zt)
    #create function to minimize. take negative of the number you get from arma_liklihood so we can minimize it
    def f(x):
        return -1*arma_likelihood(file='weather.npy',phis=x[:p],thetas=x[p:p+q],mu=x[-2],std=x[-1])
    top = np.inf
    #loop through each p and q to find the lowest AIC
    for p in range(1,i+1):
        for q in range(1,j+1):    
            x0 = np.zeros(p+q+2) 
            x0[-2] = zt.mean()
            x0[-1] = zt.std()
            sol = fmin(f,x0,maxiter=10000,maxfun=10000)
            k=p+q+2
            AIC = 2*f(sol)+2*k*(1+((k+1)/(n-k)))
            if AIC < top:
                top = AIC
                best = sol
                bp = p
                bq = q
    #return the attributes of the lowest AIC p,q
    return np.array(best[:bp]),np.array(best[bp:bp+bq]),best[-2],best[-1]
    



def arma_forecast(file='weather.npy', phis=np.array([0]), thetas=np.array([0]), mu=0., std=0., n=30):
    """
    Forecast future observations of data.
    
    Parameters:
        file (str): data file
        phis (ndarray (p,)): coefficients of AR(p)
        thetas (ndarray (q,)): coefficients of MA(q)
        mu (float): mean of ARMA model
        std (float): standard deviation of ARMA model
        n (int): number of forecast observations

    Returns:
        new_mus (ndarray (n,)): future means
        new_covs (ndarray (n,)): future standard deviations
    """
    weatherPoints = np.load(file)
    zt = weatherPoints[1:] - weatherPoints[:-1]
    N = len(zt)
    #scale for later
    start = 13.79166
    finish = 13.79166 + N*(1./24.)
    finishPred = finish + n*(1./24.)
    #get the first x and P to use in the kalman updates
    F,Q,H,dim_states,dim_time_series = state_space_rep(phis=phis, thetas=thetas, mu=mu, sigma=std)
    x,varss = kalman(F,Q,H,zt-mu)
    u,R = 0,0
    mens=[]
    covars=[]
    #kalman update then predict
    yk = zt[-1]-mu - H@x[-1]
    sk = H@varss[-1]@H.T+R
    K = varss[-1]@H.T@np.linalg.inv(sk)
    xk = x[-1] +K@yk
    Pk = (np.eye(len(K@H))-K@H)@varss[-1]
    mens.append(xk)
    covars.append(Pk)
    #predict the next n steps
    for i in range(n):
        xk1 = F@mens[i] + u
        pk1 = F@covars[i]@F.T + Q
        mens.append(xk1)
        covars.append(pk1)
    stds = np.sqrt(np.sqrt(np.mean(covars,axis = 1).mean(axis=1)))
    means = np.mean(mens,axis=1)
    stds = stds[1:]
    means = means[1:]

    pred = np.linspace(finish+(1./24.),finish+n*(1./24.),n,endpoint=True)
    #plot the predicted mean as well as two standard deviations to either side
    plt.plot(pred,means,'-r',label='forecast')
    plt.plot(pred,2*stds+means,'g',label="95% Confidence Interval")
    plt.plot(pred,-2*stds+means,'g')
    #plot the old data
    plt.plot(np.linspace(start,finish,N,endpoint=True),zt,label="Old Data",color = 'b')
    plt.ylabel("Change in Tempurature")
    plt.xlabel("Day of the Month")
    plt.title("Confidence intervals using ARMA")
    plt.gca().legend()
    plt.show()
    return means,stds


def sm_arma(file = 'weather.npy', p=4, q=4, n=30):
    """
    Build an ARMA model with statsmodel and 
    predict future n values.

    Parameters:
        file (str): data file
        p (int): maximum order of autoregressive model
        q (int): maximum order of moving average model
        n (int): number of values to predict

    Return:
        aic (float): aic of optimal model
    """
    weatherPoints = np.load(file)
    zt = weatherPoints[1:] - weatherPoints[:-1]
    #get the length of the time series
    N = len(zt)
    #scale for later 
    start = 13.79166
    finish = 13.79166 + N*(1./24.)
    lowest = np.inf
    for i in range(1,p+1):
        for j in range(1,q+1):
            model = ARMA(zt,order=(i,j))
            fitted = model.fit(method='mle',trend='c')
            #find the qp values that have the lowest AIC
            if fitted.aic < lowest:
                best = fitted
                lowest = fitted.aic
                bq = j
                bp = i
    
    #plot the ARMA model as well as the data
    plt.plot(np.linspace(start,finish,N,endpoint=True),zt,label="Old Data",color = 'b')
    plt.plot(np.linspace(start,finish+n*(1./24.),N+n+1,endpoint=True),best.predict(start=0,end=N+n),'orange',label="ARMA model")
    plt.ylabel("Change in Tempurature")
    plt.xlabel("Day of the Month")
    plt.title("Statsmodel ARMA({},{})".format(bp,bq))
    plt.gca().legend()
    plt.show()
    return lowest
    

def manaus(start='1983-01-31',end='1995-01-31',p=4,q=4):
    """
    Plot the ARMA(p,q) model of the River Negro height
    data using statsmodels built-in ARMA class.

    Parameters:
        start (str): the data at which to begin forecasting
        end (str): the date at which to stop forecasting
        p (int): max_ar parameter
        q (int): max_ma parameter
    Return:
        aic_min_order (tuple): optimal order based on AIC
        bic_min_order (tuple): optimal order based on BIC
    """
    # Get dataset
    raw = pydata('manaus')
    # Make DateTimeIndex
    manaus = pd.DataFrame(raw.values,index=pd.date_range('1903-01','1993-01',freq='M'))
    manaus = manaus.drop(0,axis=1)
    # Reset column names
    manaus.columns = ['Water Level']
    #get the q and p for aic and bic
    order = order_select(manaus.values,max_ar=p,max_ma=q,ic=['aic','bic'],fit_kw={'method':'mle'})
    aic_order = order['aic_min_order']
    bic_order = order['bic_min_order']

    fig,ax = plt.subplots(2,1,figsize=(13,7))
    #graph the confidence intervals for both models
    model = ARMA(manaus,aic_order).fit(method='mle')
    model.plot_predict(start=start,end=end,ax=ax[0])
    ax[0].set_title('AIC')
    ax[0].set_xlabel('Year')
    ax[0].set_ylabel('Water Level')

    model = ARMA(manaus,bic_order).fit(method='mle')
    model.plot_predict(start=start,end=end,ax=ax[1])
    ax[1].set_title('BIC')
    ax[1].set_xlabel('Year')
    ax[1].set_ylabel('Water Level')
    plt.tight_layout()
    plt.suptitle('Water Levels in the Rio Negro',y=1.04)

    plt.show()
    return


    

###############################################################################
    
def kalman(F, Q, H, time_series):
    # Get dimensions
    dim_states = F.shape[0]

    # Initialize variables
    # covs[i] = P_{i | i-1}
    covs = np.zeros((len(time_series), dim_states, dim_states))
    mus = np.zeros((len(time_series), dim_states))

    # Solve of for first mu and cov
    covs[0] = np.linalg.solve(np.eye(dim_states**2) - np.kron(F,F),np.eye(dim_states**2)).dot(Q.flatten()).reshape(
            (dim_states,dim_states))
    mus[0] = np.zeros((dim_states,))

    # Update Kalman Filter
    for i in range(1, len(time_series)):
        t1 = np.linalg.solve(H.dot(covs[i-1]).dot(H.T),np.eye(H.shape[0]))
        t2 = covs[i-1].dot(H.T.dot(t1.dot(H.dot(covs[i-1]))))
        covs[i] = F.dot((covs[i-1] - t2).dot(F.T)) + Q
        mus[i] = F.dot(mus[i-1]) + F.dot(covs[i-1].dot(H.T.dot(t1))).dot(
                time_series[i-1] - H.dot(mus[i-1]))
    return mus, covs

def state_space_rep(phis, thetas, mu, sigma):
    # Initialize variables
    dim_states = max(len(phis), len(thetas)+1)
    dim_time_series = 1 #hardcoded for 1d time_series

    F = np.zeros((dim_states,dim_states))
    Q = np.zeros((dim_states, dim_states))
    H = np.zeros((dim_time_series, dim_states))

    # Create F
    F[0][:len(phis)] = phis
    F[1:,:-1] = np.eye(dim_states - 1)
    # Create Q
    Q[0][0] = sigma**2
    # Create H
    H[0][0] = 1.
    H[0][1:len(thetas)+1] = thetas

    return F, Q, H, dim_states, dim_time_series