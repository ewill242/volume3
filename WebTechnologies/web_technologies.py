# solutions.py
"""Volume 3: Web Technologies. Solutions File.
Ethan Williams
section 2?
september 10th
"""
import re
import json
import socket
import matplotlib.pyplot as plt
import collections
import numpy as np
import time
import random


# Problem 1
def prob1(filename="nyc_traffic.json"):
    """Load the data from the specified JSON file. Look at the first few
    entries of the dataset and decide how to gather information about the
    cause(s) of each accident. Make a readable, sorted bar chart showing the
    total number of times that each of the 7 most common reasons for accidents
    are listed in the data set.
    """
    with open(filename,'r') as infile:
        traffic_data = json.load(infile) #load the json data into traffic_data
    reasons = collections.Counter()
    reasonsmatch = re.compile(r"^contributing") #sick regular expression to match to any key that starts with contributing
    for entry in traffic_data:
        for key in entry.keys():
            if bool(reasonsmatch.search(key)) == True: #if the regular expression did match, throw that bad boy in counter
                reasons[entry[key]] += 1
    graphed = reasons.most_common(7)
    strreas = []
    heights = []
    for item in graphed:
        strreas.append(item[0])
        heights.append(item[1])
    plt.barh(np.arange(7),heights)
    plt.yticks(np.arange(7),strreas,fontsize=8)
    plt.title("Reasons for Car Accidents")
    plt.tight_layout()
    plt.show()

    
        
    
    

class TicTacToe:
    def __init__(self):
        """Initialize an empty board. The O's go first."""
        self.board = [[' ']*3 for _ in range(3)]
        self.turn, self.winner = "O", None

    def move(self, i, j):
        """Mark an O or X in the (i,j)th box and check for a winner."""
        if self.winner is not None:
            raise ValueError("the game is over!")
        elif self.board[i][j] != ' ':
            raise ValueError("space ({},{}) already taken".format(i,j))
        self.board[i][j] = self.turn

        # Determine if the game is over.
        b = self.board
        if any(sum(s == self.turn for s in r)==3 for r in b):
            self.winner = self.turn     # 3 in a row.
        elif any(sum(r[i] == self.turn for r in b)==3 for i in range(3)):
            self.winner = self.turn     # 3 in a column.
        elif b[0][0] == b[1][1] == b[2][2] == self.turn:
            self.winner = self.turn     # 3 in a diagonal.
        elif b[0][2] == b[1][1] == b[2][0] == self.turn:
            self.winner = self.turn     # 3 in a diagonal.
        else:
            self.turn = "O" if self.turn == "X" else "X"

    def empty_spaces(self):
        """Return the list of coordinates for the empty boxes."""
        return [(i,j) for i in range(3) for j in range(3)
                                        if self.board[i][j] == ' ' ]
    def __str__(self):
        return "\n---------\n".join(" | ".join(r) for r in self.board)


# Problem 2
class TicTacToeEncoder(json.JSONEncoder):
    """A custom JSON Encoder for TicTacToe objects."""
    def default(self,obj):
        if not isinstance(obj, TicTacToe):
            raise TypeError("Expected Data type TicTacToe") #if we dont have a tictactoe object, raise error
        return {"dtype": "TicTacToe", "board": obj.board , "turn": obj.turn, "winner": obj.winner} #return the dictionary with all the attributes of the tictactoe thing


# Problem 2
def tic_tac_toe_decoder(obj):
    """A custom JSON decoder for TicTacToe objects."""
    if "dtype" in obj: #check the datatype of the given data
        if obj["dtype"] == "TicTacToe": #if its tic tac toe, we continue
            tic_board = TicTacToe()
            tic_board.board = obj["board"]
            tic_board.winner = obj["winner"] #recreate the board using the new data
            tic_board.turn = obj["turn"]
            return tic_board
        raise ValueError("Expected datatype TicTacToe")
    raise ValueError("Expected a TicTacToe JSON")

        


def mirror_server(server_address=("0.0.0.0", 33333)):
    """A server for reflecting strings back to clients in reverse order."""
    print("Starting mirror server on {}".format(server_address))

    # Specify the socket type, which determines how clients will connect.
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.bind(server_address)    # Assign this socket to an address.
    server_sock.listen(1)               # Start listening for clients.

    while True:
        # Wait for a client to connect to the server.
        print("\nWaiting for a connection...")
        connection, client_address = server_sock.accept()

        try:
            # Receive data from the client.
            print("Connection accepted from {}.".format(client_address))
            in_data = connection.recv(1024).decode()    # Receive data.
            print("Received '{}' from client".format(in_data))

            # Process the received data and send something back to the client.
            out_data = in_data[::-1]
            print("Sending '{}' back to the client".format(out_data))
            connection.sendall(out_data.encode())       # Send data.

        # Make sure the connection is closed securely.
        finally:
            connection.close()
            print("Closing connection from {}".format(client_address))

def mirror_client(server_address=("0.0.0.0", 33333)):
    """A client program for mirror_server()."""
    print("Attempting to connect to server at {}...".format(server_address))

    # Set up the socket to be the same type as the server.
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_sock.connect(server_address)    # Attempt to connect to the server.
    print("Connected!")

    # Send some data from the client user to the server.
    out_data = input("Type a message to send: ")
    client_sock.sendall(out_data.encode())              # Send data.

    # Wait to receive a response back from the server.
    in_data = client_sock.recv(1024).decode()           # Receive data.
    print("Received '{}' from the server".format(in_data))

    # Close the client socket.
    client_sock.close()


# Problem 3
def tic_tac_toe_server(server_address=("0.0.0.0", 44443)):
    """A server for playing tic-tac-toe with random moves."""
    print("Starting TicTacToe server on {}".format(server_address))

    # Specify the socket type, which determines how clients will connect.
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.bind(server_address)    # Assign this socket to an address.
    server_sock.listen(1)               # Start listening for clients.

    while True:
        # Wait for a client to connect to the server.
        connection, client_address = server_sock.accept()
        try:
            # Receive data from the client.
            in_data = connection.recv(1024).decode()    # Receive data.
            board = json.loads(in_data,object_hook=tic_tac_toe_decoder) #rebuild the board
            if board.winner is not None: #check if there was a winner in the last turn
                connection.sendall("WIN".encode())
                connection.close()
            elif board.empty_spaces() == [] and board.winner == None: #check if there was a draw due to no spaces left and no winner
                connection.sendall("DRAW".encode())
                connection.close()
            else:
                spaces = board.empty_spaces() #get the list of tuples available as spaces
                move = random.choice(spaces) #chose a random empty space
                board.move(move[0],move[1])
                if board.winner is not None: #check after you move to see if you won
                    connection.sendall("LOSE".encode())
                    connection.sendall(json.dumps(board,cls=TicTacToeEncoder).encode())
                    connection.close()
                else:
                    connection.sendall(json.dumps(board,cls=TicTacToeEncoder).encode()) #send the encoded board if nothing else
    
        finally:
            connection.close() #close the connection
            


# Problem 4
def tic_tac_toe_client(server_address=("0.0.0.0", 44443)):
    """A client program for tic_tac_toe_server()."""
    
    board = TicTacToe() #make the first board
    while True:
        print(board) #show the board to the player at the beginning of each iteration
        while True:
                try: #only accept a space thats empty  and only accept integers and stuff
                    new_space = input("Choose a space to use:")
                    arrayinput = new_space.split()
                    if len(arrayinput) != 2:
                        raise ValueError
                    righttuple = tuple((int(arrayinput[0]),int(arrayinput[1])))
                    if righttuple in board.empty_spaces(): #verify the spot is free
                        break
                except:
                    print("Please enter two ints separated by a space") #message if an incorrect string is given
                    continue
        board.move(righttuple[0],righttuple[1])
        client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_sock.connect(server_address)
        newdata = json.dumps(board,cls=TicTacToeEncoder) 
        client_sock.sendall(newdata.encode()) #send the new json data
        reply = client_sock.recv(1024).decode() #immediately wait for the turn of the server
        
        if reply == "WIN":
            print("Congrats, you win")
            break
        elif reply == "DRAW":
            print("It was a draw, better luck next time")
            break
        elif reply == "LOSE":
            print("Wrong move, Brother, YOU LOSE.") #hulk hogan
            in_data = client_sock.recv(1024).decode()
            board = json.loads(in_data,object_hook=tic_tac_toe_decoder)
            print(board)
            break
        else:
            board = json.loads(reply,object_hook=tic_tac_toe_decoder) #if the reply wasnt a string matching any, it was the board
            client_sock.close()

    
