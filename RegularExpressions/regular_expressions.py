# regular_expressions.py
"""Volume 3: Regular Expressions.
<Name> Ethan Williams
<Class> Math 403
<Date> september 3rd
"""
import re

# Problem 1
def prob1():
    """Compile and return a regular expression pattern object with the
    pattern string "python".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    pattern = re.compile("python") #gives a regex that searches for python
    return pattern

# Problem 2
def prob2():
    """Compile and return a regular expression pattern object that matches
    the string "^{@}(?)[%]{.}(*)[_]{&}$".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    meta = re.compile(r"\^\{\@\}\(\?\)\[\%\]\{\.\}\(\*\)\[\_\]\{\&\}\$") #returns the exact symbols desired
    return meta

# Problem 3
def prob3():
    """Compile and return a regular expression pattern object that matches
    the following strings (and no other strings).

        Book store          Mattress store          Grocery store
        Book supplier       Mattress supplier       Grocery supplier

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    third = re.compile(r"^(Book|Mattress|Grocery) (store|supplier)$") #returns any combination of book,mattress,grocery at the beginning
    #and store or supplier at the end. only a space is allowed in between
    return third

# Problem 4
def prob4():
    """Compile and return a regular expression pattern object that matches
    any valid Python identifier.

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    identifier = re.compile(r"^[a-zA-Z_](\w|_|\b)*$") #matches to any python identifier
    parameter = re.compile(r"^[a-zA-Z_](\w|_)*[ ]*=?[ ]*('[^']*'|\d*\.?\d*|[a-zA-Z_](\w|_)*)?$") #matches to any python parameter 
    return parameter

# Problem 5
def prob5(code):
    """Use regular expressions to place colons in the appropriate spots of the
    input string, representing Python code. You may assume that every possible
    colon is missing in the input string.

    Parameters:
        code (str): a string of Python code without any colons.

    Returns:
        (str): code, but with the colons inserted in the right places.
    """
    #for each line, needed, notneeded and maybe divide the lines into groups that are then 
    #rearranged with a colon before the newline
    needed = re.compile(r"(\n*)(if|elif|for|while|def|class|with)([^\n]*)\n", re.MULTILINE | re.DOTALL) 
    notneeded = re.compile(r"(\n*)(else|finally|try)\n", re.MULTILINE | re.DOTALL )
    maybe = re.compile(r"(\n*)(except)([^\n]*)\n",re.MULTILINE | re.DOTALL)
    new = needed.sub(r"\1\2\3:\n", code) #puts a colon before the newline character
    new = notneeded.sub(r"\1\2:\n", new) #puts a colon before the newline character
    new = maybe.sub(r"\1\2\3:\n", new) #puts a colon before the newline character
    return new

# Problem 6
def prob6(filename="fake_contacts.txt"):
    """Use regular expressions to parse the data in the given file and format
    it uniformly, writing birthdays as mm/dd/yyyy and phone numbers as
    (xxx)xxx-xxxx. Construct a dictionary where the key is the name of an
    individual and the value is another dictionary containing their
    information. Each of these inner dictionaries should have the keys
    "birthday", "email", and "phone". In the case of missing data, map the key
    to None.

    Returns:
        (dict): a dictionary mapping names to a dictionary of personal info.
    """
    info = {} #create the dictionary to be returned
    regname = re.compile(r"^([a-zA-Z]+ [a-zA-Z]*\.? ?[a-zA-Z]+)") #regex for the names, emails, birthdays and phone numbers
    regemail = re.compile(r"([\w\.]*@\w*\.\w*\.?\w*)")
    regbirthday = re.compile(r"(\d?\d)[-_/](\d?\d)[-_/]\d?\d?(\d\d)")
    regphone = re.compile(r"\d?[-\.\(\)_]*(\d?\d?\d?)[-\.\(\)_]*(\d\d\d)[-\.\(\)_]*(\d\d\d\d)")
    with open(filename) as contact: #open the file then go line by line extracting the information
        for line in contact:
            #if bool(regname.search(line))==True:
            names = regname.findall(line)
            names = names[0] #makes the first (only) item in the list the value of name
            if bool(regemail.search(line))==True: #only change the email value if its found in the line
                emails = regemail.findall(line)
            else:
                emails = None #change all the values not found in the line to None
            if bool(regbirthday.search(line))==True:
                births = regbirthday.findall(line)
            else:
                births = None
            if bool(regphone.search(line))==True:
                phones = regphone.findall(line)
            else:
                phone = None
            if type(emails) == list: #reformat the information into the way we want it to look
                emails = str(emails[0])
            if type(births) == list:
                births = births[0]
                births = str(births[0])+"/"+str(births[1])+"/20"+str(births[2])
                yam = re.compile(r"^(\d)(/\d\d/\d\d\d\d)")
                births = yam.sub(r"0\1\2",births)
                
            if type(phones)==list:
                phones = phones[0]
                phones = "("+str(phones[0])+")"+str(phones[1])+"-"+str(phones[2])
            info[names] = {'birthday':births,'email':emails,'phone':phones} #create a dictionary entry for info whose value is a dictionary
    return info
            


    
