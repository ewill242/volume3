# solutions.py

import pyspark
from pyspark.sql import SparkSession
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from pyspark.ml import Pipeline
from pyspark.ml.feature import VectorAssembler, StringIndexer, OneHotEncoderEstimator
from pyspark.ml.tuning import ParamGridBuilder, TrainValidationSplit
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.evaluation import MulticlassClassificationEvaluator as MCE


# import os

# os.environ['PYSPARK_SUBMIT_ARGS'] = "--master mymaster --total-executor 2 --conf spark.driver.extraJavaOptions=-Dhttp.proxyHost=proxy.mycorp.com-Dhttp.proxyPort=1234 -Dhttp.nonProxyHosts=localhost|.mycorp.com|127.0.0.1 -Dhttps.proxyHost=proxy.mycorp.com -Dhttps.proxyPort=1234 -Dhttps.nonProxyHosts=localhost|.mycorp.com|127.0.0.1 pyspark-shell"
# ---------------------------- Helper Functions ---------------------------- #

def plot_crime(data, category):
    """
    Helper function that plots the crime and income data from problem 4.
    Parameters:
        data ((n, 3) nparray, dtype=str (or '<U22')); format should be:
            First Column:   borough name
            Second Column:  crime rate (i.e. avg crimes per month)
            Third Column:   category (i.e. minor category) of crime
    Returns: None
    """
    domain = np.arange(len(data))
    fig, ax = plt.subplots()

    # plot number of months with
    p1 = ax.plot(domain, data[:, 1].astype(float) , color='red', label='Months w/ 1+ {}'.format(category))

    # create and plot on second axis
    ax2 = ax.twinx()
    p2 = ax2.plot(domain, data[:, 2].astype(float), color='green', label='Median Income')

    # create legend
    plots = p1 + p2
    labels = [line.get_label() for line in plots]
    ax.legend(plots, labels, loc=0)

    plt.title('Months w/ 1+ {} with Median Income'.format(category))

    plt.show()



# --------------------- Resilient Distributed Datasets --------------------- #

### Problem 1
def word_count(filename='huck_finn.txt'):
    """
    A function that counts the number of occurrences unique occurrences of each
    word. Sorts the words by count in descending order.
    Parameters:
        filename (str): filename or path to a text file
    Returns:
        word_counts (list): list of (word, count) pairs for the 20 most used words
    """
    # start the SparkSession
    spark = SparkSession.builder.appName('app_name').getOrCreate()
    # load the file as an RDD

    huck = spark.sparkContext.textFile(filename)
    huck = huck.flatMap(lambda row: row.split())
    # count the number of occurances of each word
    numwords = huck.map(lambda row: (row,1))
    # sort the words by count, in descending order
    numwords = numwords.reduceByKey(lambda x,y: x+y)
    sorted = numwords.sortBy(lambda row: row[1],ascending=False).collect()
    wordlist = sorted[:20]
    # end the SparkSession
    spark.stop()
    # return a list of the (word,count) pairs for the 20 most used words
    return wordlist

### Problem 2
def monte_carlo(n=10**5, parts=6):
    """
    Runs a Monte Carlo simulation to estimate the value of pi.
    Parameters:
        n (int): number of sample points per partition
        parts (int): number of partitions
    Returns:
        pi_est (float): estimated value of pi
    """
    # start the SparkSession
    spark = SparkSession.builder.appName('app_name').getOrCreate()
    # initialize an RDD with n*parts sample points and parts partitions
    nums = spark.sparkContext.parallelize(np.random.uniform(-1,1,(n*parts,2)),parts)
    incircle = nums.map(lambda row: np.sqrt(row[0]**2+row[1]**2))
    incircle = incircle.map(lambda elem: int(elem<1))
    est = incircle.reduce(lambda x,y: x+y)/(n*parts)
    est*=4
    # end the SparkSession
    spark.stop()
    # return your estimate
    return est


# ------------------------------- DataFrames ------------------------------- #

### Problem 3
def titanic_df(filename='titanic.csv'):
    """
    Extracts various statistics from the titanic dataset. The dataset has the
    following structure:
    Survived | Class | Name | Sex | Age | Siblings/Spouses Aboard | Parents/Children Aboard | Fare
    Returns:
        """
    # start the SparkSession
    spark = SparkSession.builder.appName('app_name').getOrCreate()
    # load the dataset into a DataFrame
    schema = ('survived INT, pclass INT, name STRING, sex STRING, age FLOAT, sibsp INT, parch INT, fare FLOAT')
    titanic = spark.read.csv(filename,schema=schema)
    # how many males/females onboard
    sextable = titanic.groupBy("sex").sum("survived")
    numtable = titanic.groupBy("sex").count()
    numwomen = sextable.collect()[0][1]
    nummen = sextable.collect()[1][1]
    # extract the female and male survival rates
    ratewomen = numwomen/numtable.collect()[0][1]
    ratemen = numwomen/numtable.collect()[1][1]
    # end the SparkSession
    spark.stop()
    # # return results
    return numwomen,nummen,ratewomen,ratemen

### Problem 4
def crime_and_income(f1='london_crime_by_lsoa.csv',
                     f2='london_income_by_borough.csv', min_cat='Murder'):
    """
    Explores crime by borough and income for the specified min_cat
    Parameters:
        f1 (str): path to csv file containing crime dataset
        f2 (str): path to csv file containing income dataset
        min_cat (str): crime minor category to analyze
    returns:
        list: borough names sorted by percent months with crime, descending
    """
    # start the SparkSession
    spark = SparkSession.builder.appName('app_name').getOrCreate()
    # load the two files as PySpark DataFrames
    londoncrime = spark.read.csv(f1, header=True, inferSchema=True)
    londonincome = spark.read.csv(f2, header=True, inferSchema=True)
    # create a new dataframe containing specified values
    newframe = londoncrime.join(londonincome, on='borough').select('borough','minor_category','median-08-16',)

    newframe = newframe.groupBy('borough','minor_category').count()
    totalcrimes = newframe.groupBy('borough').sum('count')
    # totalcrimes.show(5)
    # totalmins = newframe.filter("'minor_category' == '{}'".format(min_cat))
    # totalmins.show(5)
    # joinedcrime = totalcrimes.join(totalmins,on='borough')
    # joinedcrime.show(25)
    # order dataframe by average crime rate, descending
    # newframe.groupBy('borough').sum(min_cat).show(5)
    # format the data to match the input of plot_crime()
    # data =

    # stop the SparkSession
    spark.stop()

    # plot_crime(data, min_cat)

    return totalcrimes

### Problem 5
def titanic_classifier(filename='titanic.csv'):
    """
    Implements a logistic regression to classify the Titanic dataset.
    Parameters:
        filename (str): path to the dataset
    Returns:
        lr_metrics (list): a list of metrics gauging the performance of the model
            ('accuracy', 'weightedPrecision', 'weightedRecall')
    """
    # start the SparkSession

    # load the data

    # clean up the data

    # vectorize the features

    # split test and train data
    train, test = feature_vector.randomSplit([0.75, 0.25], seed=11)

    # initialize logistic regression object

    # train the model

    # make predictions

    # obtain performance metrics
    metrics = ['accuracy', 'weightedPrecision', 'weightedRecall']

    # stop the SparkSession

    # return metric values in order from above
    return
