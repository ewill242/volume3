"""Volume 3: Web Scraping.
<Name> Ethan Williams
<Class> Section 1?
<Date> september 17th
"""
import requests
import os.path
import re
from bs4 import BeautifulSoup as bs
import numpy as np
import matplotlib.pyplot as plt
# Problem 1
def prob1():
    """Use the requests} library to get the HTML source for the website 
    example.
    Save the source as a file called example.html.
    If the file already exists, do not scrape the website or overwrite the file.
    """
    if os.path.exists("example.html"): #if the file exists, raise error
        raise ValueError("this file already exists")
    else:
        scraped = requests.get("https://example.com") #request the data
        f = open("example.html","w+") #write the html file
        f.write(scraped.text)
        f.close()
    
# Problem 2
def prob2():
    """Examine the source code of http://www.example.com. Determine the names
    of the tags in the code and the value of the 'type' attribute associated
    with the 'style' tag.

    Returns:
        (set): A set of strings, each of which is the name of a tag.
        (str): The value of the 'type' attribute in the 'style' tag.
    """
    with open("example.html","r") as example:
        contents = example.read()
    gettingtags = re.compile(r"<(\w+)")
    removebrack = re.compile(r"(w+)") #clean the data with this
    listofheads = []
    headers = gettingtags.findall(contents) #get all the tags that match the first regex
    for tag in headers:
        listofheads.append(removebrack.sub(r"\1",tag)) #cleans the data
    return set(listofheads), str("text/css")
    


# Problem 3
def prob3(code):
    """Return a list of the names of the tags in the given HTML code."""
    tags = bs(code,'html.parser')
    return [tag.name for tag in tags.find_all()] #return the tagnames in a list


# Problem 4
def prob4(filename="example.html"):
    """Read the specified file and load it into BeautifulSoup. Find the only
    <a> tag with a hyperlink and return its text.
    """
    with open(filename,"r") as r:
        contents = r.read()
    parsed = bs(contents,"html.parser")
    for tag in parsed.find_all('a',href=True): #find the tag that has a link
        return tag.text
        


# Problem 5
def prob5(filename="san_diego_weather.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the following tags:

    1. The tag containing the date 'Thursday, January 1, 2015'.
    2. The tags which contain the links 'Previous Day' and 'Next Day'.
    3. The tag which contains the number associated with the Actual Max
        Temperature.

    Returns:
        (list) A list of bs4.element.Tag objects (NOT text).
    """
    with open(filename,"r") as g:
        contents = g.read()
    parsed = bs(contents,"html.parser")
    day1 = re.compile(r"Previous Day") #regex to match the strings im looking for
    day2 = re.compile(r"Next Day")
    maxtemp = re.compile(r"Max Temperature")
    thursday = []
    thursday.append(parsed.find(string="Thursday, January 1, 2015").parent) #return the parent that contains this string
    thursday.append(parsed.find(string=day1).parent)
    thursday.append(parsed.find(string=day2).parent) #search for the tags with previous day and next day
    temptag = parsed.find(string=maxtemp).parent.parent.parent # find the row
    thursday.append(temptag.find(class_="wx-value")) #got to the first value in the column
    return thursday



# Problem 6
def prob6(filename="large_banks_index.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the tags containing the links to bank data from September 30, 2003 to
    December 31, 2014, where the dates are in reverse chronological order.

    Returns:
        (list): A list of bs4.element.Tag objects (NOT text).
    """
    with open(filename,"r") as g:
        contents = g.read()
    parsed = bs(contents,"html.parser")
    tags = parsed.select("table table td > a") #get the tags within table then table then td and then have a as a direct child
    return tags[1:] #return everything except the most recent one


# Problem 7
def prob7(filename="large_banks_data.html"):
    """Read the specified file and load it into BeautifulSoup. Create a single
    figure with two subplots:

    1. A sorted bar chart of the seven banks with the most domestic branches.
    2. A sorted bar chart of the seven banks with the most foreign branches.

    In the case of a tie, sort the banks alphabetically by name.
    """
    with open(filename,"r") as g:
        contents = g.read()
    parsed = bs(contents,"html.parser")
    banks = parsed.select("tbody > TR") #select the table with all the data
    sorte = []
    for t in banks:#for every item in the selected data
        val = t.select("td") #make the table data into a list
        if val[9].string == ".": #if the entry has just periods (no data) skip the entry
            continue
        newlist = [val[0].string,int(val[9].string.replace(",","")),int(val[10].string.replace(",",""))] #make a list of lists that contain the cleaned data
        sorte.append(newlist) #append the items from the table that we wanted (name of bank, domestic locations then foreign locations)
    foreign = sorte.copy()
    sorte.sort(key=lambda j: j[1])
    foreign.sort(key=lambda k: k[2])
    sorte = sorte[len(sorte)-7:]
    foreign = foreign[len(foreign)-7:]
    foreign[0], foreign[1] = foreign[1],foreign[0] #alphabetize lol
    domesticname =[]
    domesticnumber =[]
    frl =[]
    frn =[]
    for fr in foreign:
        frl.append(fr[0]) #get the names of the banks then the number associated (already alphabetized)
        frn.append(fr[2]) 
    for item in sorte:
        domesticname.append(item[0]) #get the name of the bank, then number associated
        domesticnumber.append(item[1])
    
    plt.subplot(211) #on two separate plots, graph the number of locations and the names of the banks
    plt.barh(np.arange(7),domesticnumber)
    plt.yticks(np.arange(7),domesticname)
    plt.title("Number of Domestic Locations")
    plt.subplot(212)
    plt.barh(np.arange(7),frn)
    plt.yticks(np.arange(7),frl)
    plt.title("Number of Foreign Locations")
    plt.tight_layout()
    plt.show()
    